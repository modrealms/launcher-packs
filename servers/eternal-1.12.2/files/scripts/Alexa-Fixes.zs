#Alexandra's Fixes for Eternal
#Made for Modrealms

#Import the necessary recipe providers
import mods.tconstruct.Alloy;
#Announce the script's loading
print("--- loading Alexa-Fixes.zs ---");

#Removing broken recipes
print("[Alexa-Fixes] Removing broken or conflicting recipes...");


#Conflicts with minecraft:stick
recipes.remove(<mod_lavacow:mossy_stick>);


#Remove the alloying recipe for Fluxed Electrum since it's useless
mods.tconstruct.Alloy.removeRecipe(<liquid:electrumflux>);

#Adding replacement recipes
print("[Alexa-Fixes] Adding replacement recipes...");

recipes.addShaped(<mod_lavacow:mossy_stick> * 16, [[null, null, null], [null, <ore:logWood>, null], [<ore:logWood>, null, null]]);

print("--- Alexa-Fixes.zs initialized ---");